import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import jp.alhinc.mikata_ayaka.FizzBuzz.checkFizzBuzz;

/**
 *
 */

/**
 * @author mikata.ayaka
 *
 */
public class FizzBuzzTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void FizzBuzztest1() {
		assertEquals("Fizz", checkFizzBuzz.CheckFizzBuzz(3));
	}
	@Test
	public void FizzBuzztest2() {
		assertEquals("Buzz", checkFizzBuzz.CheckFizzBuzz(5));
	}
	@Test
	public void FizzBuzztest3() {
		assertEquals("FizzBuzz", checkFizzBuzz.CheckFizzBuzz(15));
	}
	@Test
	public void FizzBuzztest4() {
		assertEquals("1", checkFizzBuzz.CheckFizzBuzz(1));
	}
}